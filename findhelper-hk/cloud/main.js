var mandrill = require("mandrill");
mandrill.initialize("dtiGtXDnpOlLtb1hV_ZJmQ");

HOIIO_APP_ID = 'kogKoKvWaNQ9D67G';
HOIIO_APP_SECRET = 'uzjqDFxAglIPoc7x';

HOIIO_APP_ID_BUYER = 'fijMDpJ1uuFIy93x';
HOIIO_APP_SECRET_BUYER = 'to9LM96Cxpexkmem';

var SellerPrivateData = Parse.Object.extend("SellerPrivateData");

// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});

Parse.Cloud.define('incrementMatchNumber', function(request, response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query(Parse.User);
  query.get(request.params.id).then(function(user){
    user.increment("floatingMatches", 3);
    user.save().then(function(){
      response.success('Successfully +1 match opportunity.'+ request.params.id)
    }, function(error){
      response.error('Error.'+error.message)
    });
  })
});

Parse.Cloud.define('floatingInterested', function(request, response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query('HelperQuery');
  query.get(request.params.id).then(function(seller){
    seller.increment("floatingInterested", request.params.add);
    seller.save().then(function(){
      response.success('Successfully ' + request.params.add + ' interested.'+ request.params.id)
    }, function(error){
      response.error('Error.'+error.message)
    });
  })
});

Parse.Cloud.define('floatingAgreedStartDate', function(request, response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query('HelperQuery');
  query.get(request.params.id).then(function(seller){
    seller.increment("floatingAgreedStartDate", request.params.add);
    seller.save().then(function(){
      response.success('Successfully ' + request.params.add + ' agreed start date number.'+ request.params.id)
    }, function(error){
      response.error('Error.'+error.message)
    });
  })
});

Parse.Cloud.define('incrementRejectionNumber', function(request, response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query("HelperQuery");
  query.get(request.params.id).then(function(user){
    user.increment("jobRejections", 1);
    user.save().then(function(){
      response.success('Successfully +1 job rejection.'+ request.params.id)
    }, function(error){
      response.error('Error.'+error.message)
    });
  })
});

Parse.Cloud.define("emailAskBuyerToChoose", function(request, response) {
  var sellerName = request.params.sellerName;
  var buyerEmail = request.params.buyerEmail;
  var buyerName = request.params.buyerName;

  mandrill.sendEmail({
    message: {
      text: "Your chosen helper, " + sellerName + " is not able to take up your job. Please login again at http://search.afteryou.co to choose other helpers!",
      subject: "Unfortunately, your chosen helper is not able to do your job.",
      from_email: "sayhello@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail,
          name: buyerName
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"+ request.params.sellerName + request.params.buyerName + request.params.buyerEmail); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailBuyerSellerContacted", function(request, response) {
  var sellerName = request.params.sellerName;
  var buyerEmail = request.params.buyerEmail;
  var buyerName = request.params.buyerName;

  mandrill.sendEmail({
    message: {
      text: "Your chosen helper, " + sellerName + " has been contacted about your job. We will notify you as soon as "+ sellerName +" is able to take up your job!",
      subject: "[AfterYou] Your helper has been contacted, awaiting their confirmation!",
      from_email: "sayhello@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail,
          name: buyerName
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"+ request.params.sellerName + request.params.buyerName + request.params.buyerEmail); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailBuyerSellerInterested", function(request, response) {
  var sellerName = request.params.sellerName;
  var sellerPhone = request.params.sellerPhone;
  var buyerEmail = request.params.buyerEmail;
  var buyerName = request.params.buyerName;

  mandrill.sendEmail({
    message: {
      text: "Your chosen helper, " + sellerName + " has confirmed your job. " + sellerName +" will you contact you via the mobile number " +sellerPhone+ " to confirm actual job time, date, final price and task to be done.",
      subject: "[AfterYou] Your helper has taken up your job!",
      from_email: "sayhello@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail,
          name: buyerName
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"+ request.params.sellerName + request.params.buyerName + request.params.buyerEmail); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailBuyerStateDateConfirmed", function(request, response) {
  var sellerName = request.params.sellerName;
  var sellerNumber = request.params.sellerName;
  var buyerEmail = request.params.buyerEmail;
  var buyerName = request.params.buyerName;

  mandrill.sendEmail({
    message: {
      text: "Your chosen helper, " + sellerName + " has confirmed your job. "+ sellerName +" will you contact you via the mobile number " +sellerNumber+ " to confirm actual job time, date, final price and task to be done.",
      subject: "[AfterYou] Your helper has taken up your job!",
      from_email: "sayhello@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail,
          name: buyerName
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"+ request.params.sellerName + request.params.buyerName + request.params.buyerEmail); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailHqSellerUpdate", function(request, response) {
  var message = request.params.message;
  mandrill.sendEmail({
    message: {
      text: message,
      subject: "[Seller Action] Please take action",
      from_email: "do-not-reply@afteryou.co",
      from_name: "FindHelper Seller",
      to: [
        {
          email: "hanfengwei@afteryou.co",
          name: "AfterYou HQ"
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"+ request.params.sellerName + request.params.buyerName + request.params.buyerEmail); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailNewUser", function(request, response) {
  var Mandrill = require("cloud/mandrillTemplateSend.js");
  Mandrill.initialize("dtiGtXDnpOlLtb1hV_ZJmQ");
  var buyerEmail = request.params.email;

  Mandrill.sendTemplate({
    template_name: "findhelper-newSignUp",
    template_content: [{
        name: "example name",
        content: "example content"
      }],
    message: {
      from_email: "do-not-reply@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("emailTemplateSellerRejected", function(request, response) {
  var Mandrill = require("cloud/mandrillTemplateSend.js");
  Mandrill.initialize("dtiGtXDnpOlLtb1hV_ZJmQ");
  var sellerName = request.params.sellerName;
  var buyerEmail = request.params.buyerEmail;
  var buyerName = request.params.buyerName;

  Mandrill.sendTemplate({
    template_name: "findhelper-seller-rejected",
    template_content: [{
        name: "example name",
        content: "example content"
      }],
    message: {
      global_merge_vars: [
        {
          "name": "sellername",
          "content": sellerName
        }
      ],
      from_email: "do-not-reply@afteryou.co",
      from_name: "AfterYou FindHelper",
      to: [
        {
          email: buyerEmail,
          name: buyerName
        }
      ]
    },
    async: true
  }, {
    success: function(httpResponse) { response.success("Email sent!"); },
    error: function(httpResponse) { response.error("Uh oh, something went wrong"); }
  });
});

Parse.Cloud.define("newRequestSmsSeller", function(request, response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query(Parse.User);
  query.get(request.params.buyerId).then(function(user){
    var buyerName = user.get('name');
    var postalCode = user.get('postal_code');
    var marketRate = '$16';
    var frequency = user.get('frequency');

    var query = new Parse.Query('HelperQuery')
    query.get(request.params.sellerId).then(function(seller){
      var sellerNumber =  seller.get('phone');
      Parse.Cloud.httpRequest(
        {
          'url': 'https://secure.hoiio.com/open/sms/send',
          'method': 'POST',
          'headers': {
            'content-type': 'application/x-www-form-urlencoded'
            },
          'body': {
            'app_id': HOIIO_APP_ID,
            'access_token': HOIIO_APP_SECRET,
            'msg': 'JOB REQUEST: '+ frequency +' cleaning for '+ buyerName +' at '+ postalCode +' @ $'+ marketRate +'/hr. Please reply 1 if you are interested so we can release the client telephone phone to you',
            'dest': "+6591725582"
          }
        }).then(function(httpResponse) {
          // success
          console.log(httpResponse.text);
          response.success("Yay! SMS to "+ sellerNumber + " " + httpResponse.text);
        },function(httpResponse) {
          // error
          console.error('Request failed with response code ' + httpResponse.status);
          response.error("Uh oh, something went wrong"+httpResponse.text);
        });
    })
  })
})

Parse.Cloud.define("sendSmsGeneral", function(request, response){
  Parse.Cloud.httpRequest(
    {
      'url': 'https://secure.hoiio.com/open/sms/send',
      'method': 'POST',
      'headers': {
        'content-type': 'application/x-www-form-urlencoded'
        },
      'body': {
        'app_id': HOIIO_APP_ID,
        'access_token': HOIIO_APP_SECRET,
        'msg': request.params.message,
        'dest': '+65'+request.params.dest
      }
    }).then(function(httpResponse) {
      // success
      console.log(httpResponse.text);
      response.success("Yay! SMS to "+ request.params.dest + " " + httpResponse.text);
    },function(httpResponse) {
      // error
      console.error('Request failed with response code ' + httpResponse.status);
      response.error("Uh oh, something went wrong"+httpResponse.text);
    });
})

Parse.Cloud.define("sendSmsGeneralBuyer", function(request, response){
  Parse.Cloud.httpRequest(
    {
      'url': 'https://secure.hoiio.com/open/sms/send',
      'method': 'POST',
      'headers': {
        'content-type': 'application/x-www-form-urlencoded'
        },
      'body': {
        'app_id': HOIIO_APP_ID_BUYER,
        'access_token': HOIIO_APP_SECRET_BUYER,
        'msg': request.params.message,
        'dest': '+65'+request.params.dest
      }
    }).then(function(httpResponse) {
      // success
      console.log(httpResponse.text);
      response.success("Yay! SMS to "+ request.params.dest + " " + httpResponse.text);
    },function(httpResponse) {
      // error
      console.error('Request failed with response code ' + httpResponse.status);
      response.error("Uh oh, something went wrong"+httpResponse.text);
    });
})

Parse.Cloud.define("avgSellerAskingPrice", function(request, response) {
  Parse.Cloud.useMasterKey();
  // Get list of prices
  var query = new Parse.Query("HelperQuery");
  // Based on last 100 asking prices
  query.limit(100);
  query.select('target_Price');
  query.descending('createdAt');
  query.find().then(function(result){
    var sum = 0;
    for ( var i = 0; i < result.length; i++){
      sum += parseInt ( result[i].get('target_Price'), 10 );
    }
    var avg = sum / result.length;
    response.success(avg)
  })
});

Parse.Cloud.define("avgTransactedPrice", function(request, response) {
  Parse.Cloud.useMasterKey();
  // Get list of prices
  var query = new Parse.Query("Job");
  query.limit(5);
  query.select('fee');
  query.notEqualTo('fee', 0);
  query.descending('createdAt');
  query.find().then(function(result){
    var sum = 0;
    for ( var i = 0; i < result.length; i++){
      sum += parseInt ( result[i].get('fee'), 10 );
    }
    var avg = sum / result.length;
    // Round nearest
    var round = Math.round(avg*2)/2
    response.success(round)
  })
});

Parse.Cloud.define("getAddressWithPostalCode", function(request, response){
  var postalCode = request.params.postalCode;
  Parse.Cloud.httpRequest(
    {
      'url': 'http://www.onemap.sg/API/services.svc/basicSearch?token=ITJllkztsmFRTzxx4DM3tbr71IPcSBvqMT/m9B1EFmcCkntiMp5cBIGlAQWPr2gl5s3Yj3V+cDXnqY9bITU+RFh3mUar0R3U|mv73ZvjFcSo=&searchVal='+postalCode+'&returnGeom=0&rset=1&getAddrDetl=Y',
      'method': 'GET'
    }
  ).then(function(httpResponse) {
    // success
    var data = JSON.parse(httpResponse.text);
    response.success(data);

  });
})

Parse.Cloud.define("checkPendingSellers", function(request,response){
  Parse.Cloud.useMasterKey();
  var query = new Parse.Query(Parse.User);
  query.get(request.params.id).then(function(user){
    user.increment("pendingSellers", -1);
    user.save().then(function(){
      response.success('pendingSellers -1: '+ request.params.id);
    }, function(error){
      response.error('Error.'+error.message);
    }).then(function(){
      if(user.pendingSellers<=0){
        user.lastActive = new Date();
        user.save();
        console.log("all sellers reject the buyer. lastActive date updated.");
      }
    });
  });
});

// Aftersave triggers
Parse.Cloud.afterSave("BuyerPrivateData", function(request){
  // Make sure admin is part of ACL
  buyerPrivate = request.object;
  var objectACL = new Parse.ACL();
  objectACL.setRoleWriteAccess("administrator", true);
  objectACL.setRoleReadAccess("administrator", true);
  objectACL.setWriteAccess(buyerPrivate.get('belongsTo'), true);
  objectACL.setReadAccess(buyerPrivate.get('belongsTo'), true);
  buyerPrivate.setACL(objectACL)
  buyerPrivate.save();
});

Parse.Cloud.afterSave("HelperQuery", function(request){
  Parse.Cloud.useMasterKey();
  // Create
  seller = request.object;
  if (seller.get('privateData')){
    // No need to save over
    console.log('Has private object already.')
  } else {
    // Make new private object
    var sellerPrivate = new SellerPrivateData();
    // Set the phone number
    sellerPrivate.set('phone', seller.get('phone'))
    sellerPrivate.set('icNumber', seller.get('icNumber'))
    // Save the private object
    sellerPrivate.save().then(function(privateObject){
      seller.set('privateData', privateObject);
      seller.unset('phone')
      seller.unset('icNumber')
      seller.save();
    })
  }

});
