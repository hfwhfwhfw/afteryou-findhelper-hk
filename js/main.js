if (location.hostname == 'localhost'){
  console.warn('On localhost, using test keys')
  STRIPE_KEY = 'pk_test_OF189VkXuDDGUiYIgqXdUMQE';
  INTERCOM_ID ='oqxci5w8';
  API_SERVER_LOCATION = 'http://localhost:8080/';
} else {
  STRIPE_KEY = 'pk_live_1cjHYUVRK3pnbotaEGLJhhUU';
  INTERCOM_ID ='y8x92dph';
  API_SERVER_LOCATION = 'http://api.afteryou.co/';
}

Stripe.setPublishableKey(STRIPE_KEY);

// Notify if user is using private browsing mode
if (typeof localStorage === 'object') {
  try {
    localStorage.setItem('localStorage', 1);
    localStorage.removeItem('localStorage');
  } catch (e) {
    Storage.prototype._setItem = Storage.prototype.setItem;
    Storage.prototype.setItem = function() {};
    alert('This website is not supported in "Private/Incognito Browsing Mode". For full website experience, access this website again in normal browsing mode.');
  }
}

var app = angular.module('AfterYouWeb', ['ngtimeago' ,'angular-loading-bar','cfp.loadingBar', 'ui.router', 'ui.bootstrap', 'ngMessages', 'ngTagsInput', 'truncate']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider, cfpLoadingBarProvider) {
  cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('/', {
        url: '/',
        templateUrl: 'partials/mainpage.html'
    })
    .state('transactions', {
        url: '/transactions',
        templateUrl: 'partials/transactions.html'
    })
    .state('pending', {
        url: '/pending',
        templateUrl: 'partials/pending.html'
    })
    .state('myProfile', {
        url: '/myprofile',
        templateUrl: 'partials/myprofile.html'
    })
    .state('logOut', {
        url: '/logout',
        template: '',
        controller: 'LogOutController'
    })
    .state('login', {
        url: '/login',
        templateUrl: 'partials/login.html'
    })
    .state('forgetPassword', {
        url: '/forgetpassword',
        templateUrl: 'partials/forgetpassword.html'
    })
    .state('faq', {
        url: '/faq',
        templateUrl: 'partials/faq.html'
    })
    .state('findHelper', {
        url: '/helpers?budget&tasks&location&additionaltasks&day',
        templateUrl: 'partials/sellerlist.html'
    })
    .state('sellerform', {
        url: '/helper/register',
        templateUrl: 'partials/sellerform.html'
    })
    .state('sellerresult', {
        url: '/helper/success',
        templateUrl: 'partials/sellerresult.html'
    })
    .state('sellerDetail', {
        url: '/helpers/:sellerId?expectedDate',
        templateUrl: 'partials/sellerdetail.html'
    });
    $locationProvider.hashPrefix('!');
    $locationProvider.html5Mode(true);
});

app.run(function($rootScope, $location, $state, Buyer, Job, $filter, $window){
  if ($location.host() == 'localhost' ){
    console.warn('On localhost: ('+'ay-findhelper'+ $location.path()+') Not sending Google Analytics tracking')
  } else {
    // Initialise GA
    $window.ga('create', 'UA-65963856-4', 'auto');
  }

  // On page change
  $rootScope.$on('$stateChangeSuccess', function (event) {
    console.log("Current location: "+ $location.host() + $location.path())

    var locationHost = $location.host();
    $window.ga('set', 'dimension1', locationHost);
    $window.ga('send', 'pageview', 'ay-findhelper'+ $location.path());
    // Update Intercom state change
    $window.Intercom('update');
  });

  Parse.initialize("cv3FhjzBsG4Gwt1T5pQVFDCTTMeNN9yP7IepqmrL", "6o9ktRgqBR4tPZuVtEFINBXjxaG4dhhTd2stNH9S");
  $rootScope.sessionUser = Parse.User.current();
  $rootScope.marketPrice = 16;

  if ($rootScope.sessionUser){
    // Intercom for logged in User
    var taskFlat = $filter('flattenTasks')($filter('translateTaskNames')($filter('arrayOfTasks')($rootScope.sessionUser.tasks)));
    window.Intercom('boot', {
      app_id: INTERCOM_ID,
      email: $rootScope.sessionUser.email,
      created_at: $rootScope.sessionUser.createdAt.getTime()/ 1000,
      name: $rootScope.sessionUser.name,
      "tasks": taskFlat,
      "phone number": $rootScope.sessionUser.phone
    });

    $window.ga('set', 'userId', $rootScope.sessionUser.id);
    Parse.User.current().fetch();
    $rootScope.sessionUser.updateActivity();
    if ($rootScope.sessionUser.postal_code){
      $rootScope.sessionUser.location = $filter("convertToRegion")($rootScope.sessionUser.postal_code)
    } else {
      console.warn('User has no postal code')
    }

  } else {
    window.intercomSettings = {
      app_id: INTERCOM_ID
    };
  }
})

app.factory('Job', function($q, $http){
  var Job = Parse.Object.extend("Job", {
    // Instance methods
    updateJob: function() {
      this.save().then(function(){
        alert('Job updated!');
      })
    }
  }, {
    // Class methods
    listAllByUser: function(user){
      var defer = $q.defer();
      var query = new Parse.Query(Job);
      query.descending('createdAt');
      query.equalTo('user', user);
      query.include('request');
      query.include('seller');
      query.find().then(function(data){
        defer.resolve(data);
      }, function(error){
        defer.reject(data);
      });
      return defer.promise;
    },
    numberOfUnpaidPayments: function(user){
      var defer = $q.defer();
      var query = new Parse.Query(Job);
      query.equalTo('user', user);
      query.equalTo('paymentStatus', 'pending');
      query.count().then(function(data){
        defer.resolve(data);
      }, function(error){
        defer.reject(data);
      });
      return defer.promise;
    }
  })

  Job.prototype.__defineGetter__("paymentStatus", function() {
      return this.get("paymentStatus");
    });
  Job.prototype.__defineSetter__("paymentStatus", function(val) {
      return this.set("paymentStatus", val);
    });
  Job.prototype.__defineGetter__("request", function() {
      return this.get("request");
    });
  Job.prototype.__defineGetter__("seller", function() {
      return this.get("seller");
    });
  Job.prototype.__defineGetter__("paymentDate", function() {
      return this.get("paymentDate");
    });
  Job.prototype.__defineGetter__("paymentId", function() {
      return this.get("paymentId");
    });
  Job.prototype.__defineGetter__("jobDate", function() {
      return this.get("jobDate");
    });
  Job.prototype.__defineGetter__("fee", function() {
      return this.get("fee");
    });
  Job.prototype.__defineGetter__("duration", function() {
      return this.get("duration");
    });

  // Prototypes
  return Job;
});

app.factory('Buyer', function($q){
  var Buyer = Parse.User.extend({
    // Instance methods
    decrementMatchNumber: function(){
      this.increment("floatingMatches", -1);
      this.save();
    },
    // Update activity
    updateActivity: function(){
      todaysDate = new Date();
      this.set("lastActive", todaysDate );
      this.save();
    }

  }, {
    // Class methods
    createUser: function(userContainer){
      var user = new Parse.User();
      var defer = $q.defer();
      user.set("name", userContainer.name);
      user.set("email", userContainer.email);
      user.set("username", userContainer.email);
      user.set("password", userContainer.pin);
      user.set("phone", userContainer.phone);
      user.set("frequency", userContainer.frequency);
      user.set("referrer", document.referrer);
      user.set("nationality_pref", userContainer.nationality_pref);
      user.set("tasks", userContainer.tasks);
      user.set("location", userContainer.location);
      user.set("pcpd", userContainer.pcpd);
      user.set("price", userContainer.price);
      user.set("displayHelperList", true);
      user.set("additionaltasks", userContainer.additionaltasks);
      // Parse signUp Process
      user.signUp().then(function(buyer){
        //  Associate GA ID
        ga('set', 'userId', buyer.id);
        var BuyerPrivateData = Parse.Object.extend("BuyerPrivateData");
        var buyerPrivate = new BuyerPrivateData();
        buyerPrivate.set('phone', userContainer.phone);
        buyerPrivate.set('belongsTo', buyer);
        var privateACL = new Parse.ACL(buyer);
        buyerPrivate.setACL(privateACL);
        // Create and save private object
        buyerPrivate.save().then(function(privateObject){
          // Save buyer again
          buyer.set('privateData', privateObject)
          buyer.save().then(function(buyer){
            // Parse.Cloud.run('emailNewUser', { email: userContainer.email, name: userContainer.name }).then(function(response){
            //   console.log(response);
            //   fbq('track', 'CompleteRegistration');
            // });
            var query = new Parse.Query(Parse.Role);
            query.equalTo("name", "buyer");
            query.first().then(function(role){
              role.relation("users").add(buyer);
              role.save().then(function(saved){
                console.log("User created and role stored");
                defer.resolve(saved);
              })
            })
          })
        })
      }, function(e){
        alert("User not created! Error: " + e.message);
        defer.reject(saved);
      });
      return defer.promise;
    }
  })

  Buyer.prototype.__defineSetter__("name", function(val) {
      return this.set("name", val);
    });
  Buyer.prototype.__defineGetter__("name", function() {
      return this.get("name");
    });

  Buyer.prototype.__defineGetter__("floatingMatches", function() {
      return this.get("floatingMatches");
    });
  Buyer.prototype.__defineGetter__("displayHelperList", function() {
      return this.get("displayHelperList");
    });
  Buyer.prototype.__defineSetter__("displayHelperList", function(val) {
      return this.set("displayHelperList", val);
    });
  Buyer.prototype.__defineSetter__("email", function(val) {
      return this.set("email", val);
    });
  Buyer.prototype.__defineGetter__("email", function() {
      return this.get("email");
    });
  Buyer.prototype.__defineSetter__("phone", function(val) {
      return this.set("phone", val);
    });
  Buyer.prototype.__defineGetter__("phone", function() {
      return this.get("phone");
    });

  Buyer.prototype.__defineSetter__("location", function(val) {
      return this.set("location", val);
    });
  Buyer.prototype.__defineGetter__("location", function() {
      return this.get("location");
    });

  Buyer.prototype.__defineSetter__("nationality_pref", function(val) {
      return this.set("nationality_pref", val);
    });
  Buyer.prototype.__defineSetter__("frequency", function(val) {
      return this.set("frequency", val);
    });
  Buyer.prototype.__defineGetter__("frequency", function() {
      return this.get("frequency");
    });

  Buyer.prototype.__defineSetter__("tasks", function(val) {
      return this.set("tasks", val);
    });
  Buyer.prototype.__defineGetter__("tasks", function() {
      return this.get("tasks");
    });

  Buyer.prototype.__defineGetter__("privateData", function() {
      return this.get("privateData");
    });

  Buyer.prototype.__defineSetter__("additionaltasks", function(val) {
      return this.set("additionaltasks", val);
    });

  Buyer.prototype.__defineSetter__("price", function(val) {
      return this.set("price", val);
    });
  Buyer.prototype.__defineSetter__("pin", function(val) {
      return this.set("password", val);
    });
  Buyer.prototype.__defineSetter__("price", function(val) {
      return this.set("price", val);
    });
  Buyer.prototype.__defineSetter__("pcpd", function(val) {
      return this.set("pcpd", val);
    });
  Buyer.prototype.__defineSetter__("lastActive", function(val) {
      return this.set("lastActive", val);
    });

  return Buyer;
})

app.factory('Seller', function($q){
  var Seller = Parse.Object.extend("HelperQuery", {
  }, {
    // Class methods
    createSeller: function(sellerContainer){
      var defer = $q.defer();
      var seller = new Seller();
      seller.set("name", sellerContainer.name);
      seller.set("phone", parseInt(sellerContainer.phone));
      // seller.set("postal_Code", parseInt(sellerContainer.postal) );
      var thisYear = new Date().getFullYear();
      seller.set("age", thisYear - sellerContainer.year);
      seller.set("yearOfBirth", sellerContainer.year.toString());
      seller.set("nationality", sellerContainer.nationality);
      seller.set("gender", sellerContainer.gender);
      seller.set("icNumber", sellerContainer.ic);
      // seller.set("hide", true);
      seller.set("target_Price", sellerContainer.price , 10);
      seller.set("tasks",sellerContainer.tasks);
      seller.set("location",sellerContainer.location);
      seller.set("commission", sellerContainer.commission);
      seller.set("pcpd", sellerContainer.pcpd);
      seller.save().then(function(result){
        defer.resolve(result);
      })
      return defer.promise;
    },
    listAll: function(){
      var defer = $q.defer();
      var query = new Parse.Query(Seller);
      query.limit(400);
      query.notEqualTo("nationality", "foreigner");
      // query.notEqualTo("hide", true);
      query.select("name", "location", "tasks", 'yearOfBirth', 'gender','target_Price', 'sellerNotes', "postal_Code", 'unresponsiveCount', 'totalSessionCount', 'totalSessionCompletedCount', 'verified', 'nationality', 'currentRequestCount');
      query.lessThan("yearOfBirth",(new Date().getFullYear()-16).toString());
      query.descending('createdAt');
      query.find().then(function(data){
        defer.resolve(data);
      }, function(data, error){
        defer.reject(data);
      });
      return defer.promise;
    },
    retrieve: function(id){
      var defer = $q.defer();
      var query = new Parse.Query(Seller);
      query.get(id).then(function(data){
        defer.resolve(data);
      }, function(error){
        defer.reject(data);
      });
      return defer.promise;
    }
  });

  Seller.prototype.__defineGetter__("location", function() {
      return this.get("location");
    });
  Seller.prototype.__defineGetter__("age", function() {
    if(this.get("yearOfBirth")!=null){
      return new Date().getFullYear() - this.get("yearOfBirth");
    }else{
      return "-";
    }
    });
  Seller.prototype.__defineGetter__("acceptsOnlinePayment", function() {
      return this.get("acceptsOnlinePayment");
    });
  Seller.prototype.__defineGetter__("name", function() {
      return this.get("name");
    });
  Seller.prototype.__defineGetter__("unresponsiveCount", function() {
    if (this.get("unresponsiveCount") != undefined ){
        return this.get("unresponsiveCount");
      } else {
        return 0
      }
    });
  Seller.prototype.__defineGetter__("totalSessionCompletedCount", function() {
      return this.get("totalSessionCompletedCount");
    });
  Seller.prototype.__defineGetter__("totalSessionCount", function() {
      return this.get("totalSessionCount");
    });
  Seller.prototype.__defineGetter__("verified", function() {
      return this.get("verified");
    });
  Seller.prototype.__defineGetter__("tasks", function() {
      return this.get("tasks");
  });
  Seller.prototype.__defineGetter__("gender", function() {
      if (this.get("gender") == 'female'){
        return 'Female';
      } else {
        return 'Male';
      }
    });
  Seller.prototype.__defineGetter__("currentRequestCount", function() {
    if (this.get("currentRequestCount") != undefined ){
      return this.get("currentRequestCount");
    } else {
      return '0'
    }

    });
  Seller.prototype.__defineGetter__("nationality", function() {
      var nat = this.get("nationality");
      if (nat == 'local'){
        return 'Singaporean';
      } else if (nat == 'pr'){
        return 'Permanent Resident'
      }
    });
  Seller.prototype.__defineGetter__("sellerNotes", function() {
    var notes = this.get("sellerNotes");
    if (notes == undefined){
      return 'Please contact me for more information.'
    } else {
      return notes
    }

    });
  Seller.prototype.__defineGetter__("price", function() {
      return this.get("target_Price");
    });

  return Seller
})

app.factory('Match', function($q){
  var Match = Parse.Object.extend("Match", {

  }, {
    // Class methods
    newMatch: function(sessionUser, seller, payment, offerPrice, date, requestNotes){
      var defer = $q.defer();
      var match = new Match();
      match.set("seller", seller);
      match.set("status", "pending");
      match.set("offerPrice", offerPrice);
      match.set("requestNotes", requestNotes);
      match.set('expectedDate', date)
      match.set("payment", payment);
      match.set("user", sessionUser); // Parse associate to current user
      match.save().then(function(result){
        defer.resolve(result);
      })
      return defer.promise;
    },
    getMatches: function(user){
      var defer = $q.defer();
      var query = new Parse.Query('Match');
      query.equalTo('user', user);
      query.select('status', 'seller');
      query.include('seller');
      query.find().then(function(data){
        defer.resolve(data);
      }, function(data, error){
        defer.reject(data);
        console.warn(error.message)
      })
      return defer.promise;
    }
  })

  Match.prototype.__defineGetter__("seller", function() {
      return this.get("seller");
    });
  Match.prototype.__defineGetter__("status", function() {
      return this.get("status");
    });
  Match.prototype.__defineGetter__("expectedDate", function() {
      return this.get("expectedDate");
    });
  Match.prototype.__defineGetter__("payment", function() {
      return this.get("payment");
    });
  Match.prototype.__defineSetter__("payment", function(val) {
      return this.set("payment", val);
    });
  Match.prototype.__defineSetter__("requestNotes", function(val) {
      return this.set("requestNotes", val);
    });
  Match.prototype.__defineSetter__("expectedDate", function(val) {
      return this.set("expectedDate", val);
    });
  return Match
})

app.factory('BuyerPrivateData', function($q){
  var BuyerPrivateData = Parse.Object.extend('BuyerPrivateData', {
    // Instance methods
  },{
    // Class methods
    getPrivateDataById: function(id){
      var query = new Parse.Query('BuyerPrivateData');
      var defer = $q.defer();
      query.select('stripeCus', 'walletAmount')
      query.get(id).then(function(result){
        defer.resolve(result);
      })
      return defer.promise;
    }
  })
  // Prototypes
  BuyerPrivateData.prototype.__defineGetter__("stripeCus", function() {
      return this.get("stripeCus");
    });
  BuyerPrivateData.prototype.__defineGetter__("walletAmount", function() {
      if ( this.get("walletAmount") == undefined ){
        return 0;
      } else {
        return this.get("walletAmount")
      }
    });
  return BuyerPrivateData;
})

app.factory('Favourite', function($q){
  var Favourite = Parse.Object.extend('Favourite', {
    // Instance methods
  },{
    // Class methods
    addFavourite: function(seller, buyer){
      var favourite = new Favourite();
      var defer = $q.defer();
      favourite.set('buyer', buyer);
      favourite.set('seller', seller);
      favourite.save().then(function(result){
        defer.resolve(result);
      })
      return defer.promise;
    },
    getFavouritesByUser: function(user){
      var query = new Parse.Query('Favourite');
      var defer = $q.defer();
      query.equalTo('buyer', user);
      query.find().then(function(result){
        var fav = [];
        angular.forEach(result, function(favourite){
          fav.push(favourite.seller.id)
        })
        defer.resolve(fav);
      })
      return defer.promise;
    }
  })
  // Prototypes
  Favourite.prototype.__defineSetter__("buyer", function(val) {
      return this.set("buyer", val);
    });
  Favourite.prototype.__defineSetter__("seller", function(val) {
      return this.set("seller", val);
    });
  Favourite.prototype.__defineGetter__("buyer", function() {
      return this.get("buyer");
    });
  Favourite.prototype.__defineGetter__("seller", function() {
      return this.get("seller");
    });
  return Favourite
})

app.controller('TransactionsController', function($filter, $window, Job, Seller, Buyer, Match, $scope, $rootScope, $state, $http, BuyerPrivateData){

  Job.listAllByUser($rootScope.sessionUser).then(function(data){
    $scope.jobs = data;
    console.log(data)
  })

  $scope.$watch('jobs', function() {
    $scope.unpaidSessions = $filter('filterByPaymentId')($scope.jobs).length
  });

  BuyerPrivateData.getPrivateDataById($rootScope.sessionUser.privateData.id).then(function(result){
    $rootScope.sessionUser.privateData.stripeCus = result.stripeCus;
    $rootScope.privateData = result;
    console.log(result)
    if ($rootScope.sessionUser.privateData.stripeCus){
      $rootScope.sessionUser.hasStripeAccount = true;
      var req = {
        method: 'POST',
        url: API_SERVER_LOCATION +'stripe/customer/retrieve',
        headers: { 'Content-Type': 'application/json' },
        data: {
          cusId: $rootScope.sessionUser.privateData.stripeCus,
          email: $rootScope.sessionUser.email
        }
      }
      $http(req).then(function(res){
        console.log(res)
        if (res.status == 200){
          //  Connection success
          if (res.data.error != 'not_found' && res.data.customer.deleted != true){
            console.log(res)
            if ($rootScope.sessionUser.privateData.stripeCus == res.data.customer.id){
              console.log('[Stripe] Valid Stripe customer found')
            }
            // Success
            console.log(res)
            if (res.data.customer.sources.data.length != 0){
              // has at least a card
              console.log('[Stripe] Card found')
              $scope.cardDetails = res.data.customer.sources.data[0]
              $rootScope.sessionUser.hasCard = true;
            } else {
              console.log('[Stripe] No card attached to user')
            }
          } else if (res.data.customer.deleted == true) {
            console.warn('[Stripe] Customer found, but deleted')
          } else {
            console.warn('[Stripe] No stripe customer found')
          }
        }
      }, function(err){
        if (err.status == -1) {

        } else {
          alert('Oops something went wrong on our side. Please refresh and try again? ')
        }
      });
    }
  })

  $rootScope.sessionUser.hasCard = false;

  //  Add card
  $scope.addCard = function(){
    Stripe.card.createToken({
      number: $scope.cardNumber,
      cvc: $scope.cardCVC,
      exp_month: $scope.cardMonth,
      exp_year: $scope.cardYear
    }, stripeResponseHandler);

    function stripeResponseHandler(status, response) {
      if (response.error) {
        console.warn(response.error)
        alert(response.error.message)
      } else {
        // Response contains id and card, which contains additional card details
        var token = response.id;
        console.log('[TOKEN]: '+ token)
        if ( $rootScope.sessionUser.hasStripeAccount != true){
          //  Create Cus with Card
          var req = {
            method: 'POST',
            url: API_SERVER_LOCATION+'stripe/customer/new',
            headers: { 'Content-Type': 'application/json' },
            data: {
              token: token,
              buyerId: $rootScope.sessionUser.id,
              buyerName: $rootScope.sessionUser.name,
              buyerEmail: $rootScope.sessionUser.email
            }
          }
        } else {
          // Update card only
          var req = {
            method: 'POST',
            url: API_SERVER_LOCATION+'stripe/customer/update',
            headers: { 'Content-Type': 'application/json' },
            data: {
              token: token,
              cusId: $rootScope.sessionUser.privateData.stripeCus
            }
          }
        }

        // Store card in Parse
        $http(req).then(function(res){
          console.log(res)
          if (res.data.status == 'success'){
            // Different messages
            alert('Your card has been saved!')
            location.reload();
          } else {
            alert('Please try again later')
          }
        }, function(err){
          alert('There was a network error. Please try again later.' + err )
        });
      }
    }
  }

  $scope.removeCard = function(){
    var req = {
      method: 'POST',
      url: API_SERVER_LOCATION + 'stripe/customer/deleteCard',
      headers: { 'Content-Type': 'application/json' },
      data: {
        cusId: $scope.privateData.stripeCus, // Stripe Cus token
        cardId: $scope.cardDetails.id // Stripe Cus token
      }
    }
    $http(req).then(function(res){
      console.log(res)
      // Different messages
      var status = res.data.status;
      if (status == 'cancelled'){
        alert('Job appears to have been paid. No charge has been made.')
        // Feedback to seller
      } else if (status == 'success'){
        alert('Card has been removed!')
        location.reload();
      }
    }, function(err){
      console.log(err)
      alert('There was a network error. Please try again later.' + err )
    });
  }

  $scope.paynow = function(job){
    if (  $rootScope.sessionUser.hasCard == false ){
      alert('Please link a credit card before making payment')
    } else {
      console.log('Payment Initiated')
      console.log($scope.privateData.stripeCus)
      var name = 'AfterYou FindHelper'
      var desc = 'Payment for '+ job.seller.name + ' for ' + job.duration + ' hours of job'
      var jobAmount = job.duration*job.fee*100;
      var req = {
        method: 'POST',
        url: API_SERVER_LOCATION + 'stripe/charges/new',
        headers: { 'Content-Type': 'application/json' },
        data: {
          customer: $scope.privateData.stripeCus, // Stripe Cus token
          jobId: job.id,
          sellerId: job.seller.id,
          amount: jobAmount,
          desc: desc,
          name: name
        }
      }
      $http(req).then(function(res){
        console.log(res)
        // Different messages
        var status = res.data.status;
        if (status == 'cancelled'){
          alert('Job appears to have been paid. No charge has been made.')
          // Feedback to seller
        } else if (status == 'success'){
          alert('Payment is sucessful!')
          $window.ga('send', 'event', 'Buyer', 'payment-success', 'stripe', jobAmount);
          location.reload();
        }
      }, function(err){
        console.log(err)
        alert('There was a network error. Please try again later.' + err )
      });
    }
  }

  // If not user go back main
  if ($rootScope.sessionUser == null){
    console.log('Not user')
    $state.go('/')
  }
})

app.controller('MyProfileController', function($scope, $rootScope, $state, $http, Buyer, BuyerPrivateData){
  // If not user go back main
  if ($rootScope.sessionUser == null){
    console.log('Not user')
    $state.go('/')
  }
})

app.controller('SetPriceController', function($scope, Buyer, $rootScope){
  $scope.set = function(isValid){
    if (isValid){
      $rootScope.sessionUser.price = $scope.price;
      $rootScope.sessionUser.save().then(function(){
        alert("Your preferred price is saved!");
      });
    } else {
    }
  }
})

app.controller('LoginController', function ($rootScope, $scope, $state){
  if ($rootScope.sessionUser != null){
    console.log('Is user')
    $state.go('findHelper')
  }

  $scope.logIn = function(){
    Parse.User.logIn($scope.email, $scope.password).then(function(user){
      $rootScope.sessionUser = user;
      location.reload();
    }, function(error, user){
      if (error.code == "201") {
        alert("Please enter the password to your account!")
      } else if (error.code == "101") {
        alert("Please check your login email and password and try again!")
      }
      console.warn(error);
    })
  }
})

app.controller('MenuController', function ($rootScope, $scope, $state, Job){
  if ($rootScope.sessionUser){
    Job.numberOfUnpaidPayments($rootScope.sessionUser).then(function(count){
      $rootScope.jobCount = count;
    })
  }
})

app.controller('LogOutController', function ($rootScope, $scope, $state){
  if ($rootScope.sessionUser){
    console.log('is user')
    Parse.User.logOut().then(function(data){
      location.reload();
    })
  } else {
    console.warn('is not user')
    $state.go('/')
  }

})

app.controller('SearchController', function($state, $scope, items, $uibModalInstance, $rootScope, $filter, $window){
  $scope.loadTags = [
    { name: 'Heavy Cleaning', value: 'h_cleaning' },
    { name: 'Light Cleaning', value: 'l_cleaning' },
    { name: 'Ironing', value: 'ironing' },
    { name: 'Laundry', value: 'wash_dry' },
    { name: 'Babysitting', value: 'babysitting' }
  ];
  if ($rootScope.sessionUser){
    $scope.location = $rootScope.sessionUser.location;
  }
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
    //  Log leaving the page
    $window.ga('send', 'pageview', 'ay-findhelper/');
  };


  $scope.submitSearch = function(isValid){
    if (isValid) {
      console.log($scope.tasks)
      var additionaltasks = [];
      angular.forEach($scope.additionaltasks, function(task){
        additionaltasks.push(task['name'])
      })

      // Logged searched task in GA
      var taskFlat = $filter('flattenTasks')($filter('translateTaskNames')($filter('arrayOfTasks')($scope.tasks)));
      console.log(taskFlat)
      ga('set', 'dimension2', taskFlat);

      console.log(additionaltasks)
      console.log("location: " + $scope.location);
      // Form params
      // Go to search page
      $uibModalInstance.close();
      $state.go('findHelper', {budget: $rootScope.budget, tasks: JSON.stringify($scope.tasks), location: $scope.location, additionaltasks: JSON.stringify(additionaltasks)})
    } else {
      // Prompt user to enter something
      $scope.errorMsg = "Please make sure you chose a task and a location."
      console.log('Form is empty')
    }
  }
})

app.controller('MainPageController', function ($rootScope, $scope, $state, $uibModal, $window){
  $scope.items = [];

  $scope.openSearch = function (budget) {
    // Log in GA as a page view when opening search
    $window.ga('send', 'pageview', 'ay-findhelper/openSearchDialog');
    $rootScope.budget = budget;
    console.log(budget)
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '../partials/search.html',
      controller: 'SearchController',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  }

})

app.controller('BuyerFormController', function($rootScope, $scope, $state, Buyer){
  if ($rootScope.sessionUser != null){
    console.log('Is user')
    $state.go('findHelper')
    $rootScope.sessionUser.fetch();
    $rootScope.sessionUser.updateActivity();
  }


  $scope.submitForm = function(isValid){
    if (isValid) {
        Buyer.createUser($scope.buyer).then(function(user){
          location.reload();
        })
      } else {
        alert("There are incomplete sections in your submission, please check again!")
      }
  }
});

app.controller('SignUpController', function($state, $scope, $rootScope, $stateParams, Buyer){
  if ($stateParams.tasks){
    console.log(JSON.parse($stateParams.tasks))
  }
  $scope.submitForm = function(isValid){
    if (isValid) {
      console.log('Create user')
      $scope.buyer.tasks = JSON.parse($stateParams.tasks)
      $scope.buyer.location = $stateParams.location;
      $scope.buyer.additionaltasks = JSON.parse($stateParams.additionaltasks);
      $scope.buyer.price = $rootScope.budget;
      $scope.button = '正在提交⋯⋯'
      Buyer.createUser($scope.buyer).then(function(user){
        location.reload();
        })
      } else {
        alert("There are incomplete sections in your submission, please check again!")
      }
  }

})

app.controller('SellerListController', function($window, $state, $rootScope, $scope, Seller, Match, Buyer, $location, $anchorScroll, $stateParams, $filter, $location, Favourite){
  Parse.Cloud.run('avgTransactedPrice').then(function(response) {
    $rootScope.marketPrice = response;
  });

  // Get the stateParams
  if ($stateParams.tasks){
    console.log('Search input = ' + $stateParams.tasks)
    $scope.filter = {
      'tasks': JSON.parse($stateParams.tasks),
      'location': $stateParams.postalcode
    };
  }

  $scope.clearFilters = function(){
    $scope.filter = {}
  }

  if($rootScope.sellers == undefined){
    // set list of sellers
    Seller.listAll().then(function(sellers){
      $rootScope.sellers = sellers;
      console.log('Total list of sellers: ' + sellers.length)
      angular.forEach(sellers, function(seller){
        seller.offerPrice = $rootScope.marketPrice
        seller.completion = 100*(seller.totalSessionCompletedCount/seller.totalSessionCount)
      })
      $rootScope.$watch('marketPrice', function(){
        angular.forEach(sellers, function(seller){
          seller.offerPrice = $rootScope.marketPrice
        })
      })

      Favourite.getFavouritesByUser($rootScope.sessionUser).then(function(result){
        $rootScope.sessionUser.favouritesId = result;
        angular.forEach(sellers, function(seller){
          if (result.indexOf(seller.id) > -1){
            console.log('found')
            seller.faved = true
          }
        })
      })
    });
  }

  $scope.nexted = 0;
  // generic shuffling function
  $scope.shuffle = function(o, toTop){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    if (toTop){
      $location.hash('top');
      $anchorScroll();
    }
    $scope.nexted++;
    $window.ga('send', 'pageview', 'ay-findhelper' + $location.path() +"/page/" + $scope.nexted);
    $window.ga('send', 'event', 'Buyer', 'nexted', $scope.nexted);
    return o;
  };

  $scope.goToAnchor = function(name){
    $location.hash(name);
    $anchorScroll();

  }
});

app.controller('SellerDetailController', function($window, $state, $rootScope, $scope, Seller, Match, Buyer, $location, $anchorScroll, $stateParams, $filter, $location, Favourite){
  Seller.retrieve($stateParams.sellerId).then(function(seller){
    $scope.seller = seller
  })

  $scope.expectedDate = new Date();

  $scope.saveSeller = function(seller){
    // Get instant feedback
    seller.faved = true;
    $window.ga('send', 'event', 'Buyer', 'favourited', seller.name);
    Favourite.addFavourite(seller, $rootScope.sessionUser).then(function(result){
      // Save in background
      var metadata = {
        seller_name: seller.name
      };
      Intercom('trackEvent', 'added-favourite', metadata);
    })
  }

  $scope.offerPriceFunction = function(isValid, seller, date, requestNotes){
    if (isValid){
      console.log('offer price: '+ seller.offerPrice)
      seller.offerCompleted = true;
      $window.ga('send', 'event', 'Buyer', 'request', seller.name, seller.offerPrice);
      Match.newMatch($rootScope.sessionUser, seller, 'Stripe', seller.offerPrice, date, requestNotes).then(function(result){
        seller.request = result;
        console.log(seller.request);
        var metadata = {
          seller_name: seller.name,
          offer_price: seller.offerPrice,
          preferred_date: date.getTime()/1000
        };
        Intercom('trackEvent', 'send request', metadata);
      });
    } else {
      console.log('No offer price')
    }
  }
});

app.controller('ForgetPasswordController', function($rootScope, $scope, $state, Buyer){
  $scope.submitForm = function(isValid){
    if (isValid) {
      var email = $scope.email;
        // Find user with such email
        Parse.User.requestPasswordReset(email, {
          success: function() {
          // Password reset request was sent successfully
          alert('A password reset link has been sent to you via email. Please check your email!')
          },
          error: function(error) {
            // Show the error message somewhere
            alert("Error: " + error.code + " " + error.message);
          }
        });
      } else {
        alert("There are incomplete sections in your submission, please check again!")
      }
  }
});

app.controller('PendingController', function($rootScope, $scope, $state, Buyer, Match){
  // Get matches of current buyer;
  Match.getMatches($rootScope.sessionUser).then(function(matches){
    $scope.matches = matches;
    console.log(matches);
  })
});

app.controller('SellerFormController',function($scope,Seller,$state){
  // $scope.sellerFormSubmitted = false;
  $scope.submitSellerForm = function(isValid){
    // $scope.sellerFormSubmitted = true;
    if(isValid){
      $scope.seller.tasks = [];
      for (var key in $scope.tasks ) {
        if ($scope.tasks[key] == true) {
          $scope.seller.tasks.push(key);
        }
      }

      $scope.seller.location = [];
      for (var key in $scope.location ) {
        if ($scope.location[key] == true) {
          $scope.seller.location.push(key);
        }
      }

      if($scope.commission == true){
        $scope.seller.commission = 'agree';
      }
      if($scope.pcpd == true){
        $scope.seller.pcpd = 'agree';
      }

      console.log($scope.seller);

      Seller.createSeller($scope.seller).then(function(){
        $state.go('sellerresult');
      })

    }else {
      alert("There are incomplete sections in your submission, please check again!")
    }
  }
});

app.filter('filterTasksAnd', function(){
  return function(sellers, selectedTasks){
    var tasks = [];
    for (var key in selectedTasks ) {
      if (selectedTasks[key] == true) {
        tasks.push(key)
      }
    }
    var out = [];
    if ( selectedTasks == undefined || selectedTasks == null ){
      // output all if empty
      out = sellers;
    } else {
      angular.forEach(sellers, function(seller){
        var isTrue = tasks.every(function(val){
              return seller.tasks.indexOf(val) >= 0
            }
          );
        if (isTrue == true){
          out.push(seller)
        }
      })
    }
    return out
  };
});

// Human readable task names
app.filter('translateTaskNames', function() {
  return function(input) {
    var translatedTasks = [];
    angular.forEach(input, function(task){
      switch (task){
        case "h_cleaning":
          translatedTasks.push('Heavy Cleaning')
          break
        case "l_cleaning":
          translatedTasks.push('Light Cleaning')
          break
        case "wash_dry":
          translatedTasks.push('Laundry')
          break
        case "ironing":
          translatedTasks.push('Ironing')
          break
        case "meal_prep":
          translatedTasks.push('Meal Preparation')
          break
        case "babysitting":
          translatedTasks.push('Babysitting')
          break
      }
    })
    return translatedTasks
  };
})

app.filter('filterByPaymentStatus', function() {
  return function(input, status) {
    var out = [];
    angular.forEach(input, function(job){
      if (job.paymentStatus == status){
        out.push(job)
      }
    })
    return out;
  }
});

app.filter('filterByPaymentId', function() {
  return function(input) {
    var out = [];
    angular.forEach(input, function(job){
      if (job.paymentId == null || job.paymentId == undefined || job.paymentId == ''){
        out.push(job)
      }
    })
    return out;
  }
});

app.filter('convertToRegion', function(){
  return function(postalCodeToRegion) {
    var region = "";
    var json = {
    	"postal_code": {
    	"north":["72","73","74","75","76","77","78","83"],
    	"northeast":["33","34","35","36","53","54","55","79","80","82"],
    	"northwest":["58","59","65","66","67","68","69","70","71"],
    	"south":["01","02","03","04","05","06","07","08","09","10","11","13","14","15","16"],
    	"east":["37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","81"],
    	"west":["12","60","61","62","63","64"],
    	"central":["17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","56","57"]
    	}
    }
    var postalcode = postalCodeToRegion.toString();
    if(postalcode.length === 5){
      postalcode = "0" + postalcode;
    }
    var code = postalcode.substring(0,2);
    if (json.postal_code.north.indexOf(code) > -1){
      region = "north";
    } else if (json.postal_code.northeast.indexOf(code) > -1){
      region = "northeast";
    } else if (json.postal_code.northwest.indexOf(code) > -1){
      region = "northwest";
    } else if (json.postal_code.south.indexOf(code) > -1){
      region = "south";
    } else if (json.postal_code.east.indexOf(code) > -1){
      region = "east";
    } else if (json.postal_code.west.indexOf(code) > -1){
      region = "west";
    } else if (json.postal_code.central.indexOf(code) > -1){
      region = "central";
    } else {
      region = "region not found";
    }
    return region
  }
})

app.filter('filterPrice', function(){
  return function(input, price) {
    var out = []
    angular.forEach(input, function(seller){
      if (input != undefined && price != null && seller.price != undefined ){
        // Input and seller location defined
        if (seller.price < price){
          out.push(seller)
        }
      } else if (price == null) {
        out.push(seller)
      } else if (seller.price == undefined){
        console.warn('[Seller issue] No seller price: ' + seller.name)
      }
    })
    return out
  }
})

app.filter('filterLocation', function(){
  return function(input, location) {
    var out = []
    angular.forEach(input, function(seller){
      if (input != undefined && location != null && seller.location != undefined ){
        // Input and seller location defined
        if (seller.location.indexOf(location) > -1){
          out.push(seller)
        }
      } else if (seller.location == undefined){
        console.warn('[Seller issue] No seller location: ' + seller.name)
      }
    })
    return out
  }
})

app.filter('filterFaved', function(){
  return function(input, showFav) {
    var out = []
    angular.forEach(input, function(seller){
      if (showFav == false | showFav == null){
        out.push(seller)
      } else {
        if (seller.faved == true) {
          out.push(seller)

        }
      }
    })
    return out
  }
})



// Human readable task names, from dictionary
app.filter('arrayOfTasks', function() {
  return function(input) {
    var translatedTasks = []
    for (var key in input ) {
      if (input[key] == true) {
        translatedTasks.push(key)
      }
    }
    return translatedTasks
  };
})

// Human readable task names, from dictionary
app.filter('flattenTasks', function() {
  return function(input) {
    var flattened = ''
    angular.forEach(input, function(task){
      flattened = flattened + task + ", "
    })
    return flattened.slice(0, -2);
  };
})

app.filter('range', function() {
  return function(input, min, max) {
    min = parseInt(min);
    max = parseInt(max);
    for (var i=min; i<=max; i++)
      input.push(i);
    return input;
  };
});
