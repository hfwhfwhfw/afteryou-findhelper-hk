module.exports = function(grunt){
  grunt.initConfig({

    jade: {
      options: {
        pretty: true
      },
      compile: {
          files: [{
            expand: true,
            cwd: "templates",
            src: "**/*.jade",
            dest: "",
            ext: ".html"
          }]
      }
    },

    watch: {
      scripts: {
        files: ['templates/**/*.jade'],
        tasks: ['jade'],
      },
    },

  });

  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-watch');
};
